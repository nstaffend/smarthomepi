#sql reset for testing
read -p "Enter your MySQL root password: " mysql_pass
mysql -u root -p$mysql_pass << EOF
drop database smarthomedb;
drop user smarthome@localhost;
quit
EOF
sudo rm /var/www/html/smarthome -r
echo "Loading from bitbucket. You will need to enter your username and password when prompted."
sudo git clone https://bitbucket.org/nstaffend/smarthomepi.git /var/www/html/smarthome
cd /var/www/html/smarthome
sudo chmod 776 exec/runCommand.php
sudo chmod 776 exec/init.php
echo "Running SQL setup scripts"
mysql -u root -p$mysql_pass < create_smarthome_user.sql
mysql --user=smarthome --password=password smarthomedb < build_tables.sql
#mysql --user=smarthome --password=password smarthomedb < load_dummy_data.sql
echo "removing files..."
sudo rm build_tables.sql
sudo rm create_smarthome_user.sql
echo "Complete. Restarting Apache. "
sudo service apache2 restart