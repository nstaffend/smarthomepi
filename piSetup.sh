#smarthome setup
#assumes Raspbian is installed. Will run all commands with sudo
echo "Starting PiSmarthome setup scripts. "
sudo apt-get update -y
echo -n "Installing apache and php5... "
sudo apt-get install apache2 apache2-doc apache2-utils -y
sudo apt-get install libapache2-mod-php5 php5 php-pear php5-xcache -y
#sudo apt-get install apache2 -y
#sudo apt-get install php5 libapache2-mod-php5 -y
echo "done."
read -p "Installing MySQL... You will be prompted to create a root password. Please enter the password that you will use now: " mysql_pass
echo "Installing mysql"
sudo apt-get install php5-mysql -y
sudo apt-get install mysql-server -y
sudo apt-get install mysql-client -y
sudo apt-get install php5-curl -y

# replace -ppassword with -pYOUR_PASSWORD_HERE


read -n 1 -p "Will clone the smarthome files to your /var/www/html/smarthomer folder, run scripts, then delete what's uneccessary to run them. Press any key to continue..."

echo "Pulling files from bitbucket..."
sudo git clone https://bitbucket.org/nstaffend/smarthomepi.git /var/www/html/smarthome
cd /var/www/html/smarthome
sudo chmod 776 exec/runCommand.php
sudo chmod 776 exec/init.php
echo "Running SQL setup scripts"
mysql -u root -p$mysql_pass < create_smarthome_user.sql
mysql --user=smarthome --password=password smarthomedb < build_tables.sql
#mysql --user=smarthome --password=password smarthomedb < load_dummy_data.sql
echo "removing files..."
sudo rm build_tables.sql
sudo rm create_smarthome_user.sql
echo "Restarting Apache. "
sudo service apache2 restart